// ---------------------------------------------------------------- //
// Arduino Ultrasoninc Sensor HC-SR04
// Original by Arbi Abdul Jabbaar
//Re-written by Quinton Brooks
// Using Arduino IDE 1.8.13
// Using 3 HC-SR04 Modules
// Tested on 17 April 2021
// ---------------------------------------------------------------- //

#define echoPin_left 2  // attach pin D2 Arduino to pin Echo of HC-SR04
#define trigPin_left 3  // attach pin D3 Arduino to pin Trig of HC-SR04
//#define echoPin_front 8 // attach pin D2 Arduino to pin Echo of HC-SR04
//#define trigPin_front 7 // attach pin D3 Arduino to pin Trig of HC-SR04
//#define echoPin_right 5 // attach pin D2 Arduino to pin Echo of HC-SR04
//#define trigPin_right 4 // attach pin D3 Arduino to pin Trig of HC-SR04

// defines variables
long duration; // variable for the duration of sound wave travel
int distance; // variable for the distance measurement

void setup() {
  pinMode(trigPin_left, OUTPUT); // Sets the trigPin as an OUTPUT
  pinMode(echoPin_left, INPUT); // Sets the echoPin as an INPUT
  Serial.begin(115200); // // Serial Communication is starting with 9600 of baudrate speed
  Serial.println("Ultrasonic Sensor HC-SR04 Test"); // print some text in Serial Monitor
  Serial.println("with Arduino UNO R3");
}
void loop() {
  // Clears the trigPin condition
  digitalWrite(trigPin_left, LOW);
  delayMicroseconds(2);
  // Sets the trigPins HIGH (ACTIVE) for 10 microseconds
  digitalWrite(trigPin_left, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin_left, LOW);
  // Reads the echoPin, returns the sound wave travel time in microseconds
  duration = pulseIn(echoPin_left, HIGH);
  // Calculating the distance
  distance = duration * 0.034 / 2; // Speed of sound wave divided by 2 (go and back)
  // Displays the distance on the Serial Monitor
  Serial.print("Distance: ");
  Serial.print(distance);
  Serial.println(" cm");
}
