// ---------------------------------------------------------------- //
// Arduino Ultrasoninc Sensor HC-SR04
// Original by Arbi Abdul Jabbaar
//Re-written by Quinton Brooks
// Using Arduino IDE 1.8.13
// Using 3 HC-SR04 Modules
// Tested on 17 April 2021
// ---------------------------------------------------------------- //

#define echoPin_left 2  // attach pin D2 Arduino to pin Echo of HC-SR04
#define trigPin_left 3  // attach pin D3 Arduino to pin Trig of HC-SR04
#define echoPin_front 8 // attach pin D2 Arduino to pin Echo of HC-SR04
#define trigPin_front 7 // attach pin D3 Arduino to pin Trig of HC-SR04
#define echoPin_right 5 // attach pin D2 Arduino to pin Echo of HC-SR04
#define trigPin_right 4 // attach pin D3 Arduino to pin Trig of HC-SR04

// defines variables
long duration; // variable for the duration of sound wave travel
int distance; // variable for the distance measurement
long LeftSensor, FrontSensor, RightSensor;

void setup() {
  pinMode(trigPin_left, OUTPUT); // Sets the trigPin as an OUTPUT
  pinMode(echoPin_left, INPUT); // Sets the echoPin as an INPUT
  pinMode(trigPin_front, OUTPUT); // Sets the trigPin as an OUTPUT
  pinMode(echoPin_front, INPUT); // Sets the echoPin as an INPUT
  pinMode(trigPin_right, OUTPUT); // Sets the trigPin as an OUTPUT
  pinMode(echoPin_right, INPUT); // Sets the echoPin as an INPUT
  Serial.begin(115200); // // Serial Communication is starting with 115200 of baudrate speed
  Serial.println("Ultrasonic Sensor HC-SR04 Test"); // print some text in Serial Monitor
  Serial.println("with Arduino UNO R3");
}
void loop() {

SonarSensor(trigPin_left, echoPin_left);
LeftSensor = distance;

SonarSensor(trigPin_front, echoPin_front);
FrontSensor = distance;

SonarSensor(trigPin_right, echoPin_right);
RightSensor = distance;

  // Displays the distance on the Serial Monitor
  Serial.print("LeftSensor: "); 
  Serial.print(LeftSensor);
  Serial.print("cm");
  Serial.print("   ");
  Serial.print("FrontSensor: ");
  Serial.print(FrontSensor);
  Serial.print("cm");
  Serial.print("   ");
  Serial.print("RightSensor: "); 
  Serial.print(RightSensor);
  Serial.println("cm");
  
//stop/slow signals for Left sensor---------
if (LeftSensor<30 && LeftSensor>=21)
  {
    Serial.println("SLOW LEFT");
  }
if (LeftSensor <= 20)
  {
    Serial.println("STOP LEFT");
    }
    //stop/slow signals for front sensor---------
if (FrontSensor<30 && FrontSensor>=21)
  {
    Serial.println("SLOW FRONT");
  }
if (FrontSensor <= 20)
  {
    Serial.println("STOP FRONT");
    }
    //stop/slow signals for right sensor---------
if (RightSensor<30 && RightSensor>=21)
  {
    Serial.println("SLOW RIGHT");
  }
if (RightSensor <= 20)
  {
    Serial.println("STOP RIGHT");
    }

}


void SonarSensor (int trigPin,int echoPin){ 
  // Clears the trigPin condition
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  // Sets the trigPins HIGH (ACTIVE) for 10 microseconds
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  // Reads the echoPin, returns the sound wave travel time in microseconds
  duration = pulseIn(echoPin, HIGH);
  // Calculating the distance
  distance = (duration/2) / 29.1; // Speed of sound wave divided by 2 (go and back)
}
